<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Тестовая корзина</title>
</head>
<body>
        <?php foreach ($data as $viewProduct):?>
            <div>
            <a href="?id=<?=$viewProduct['id']?>"><?=$viewProduct['title']?></a>
            </div>
        <?php endforeach;?>

        <hr>
        <div>
            <div>Товары в корзине</div>
            <?php foreach ($product->getProducts() as $cart):?>
                <div>
                 <?=$cart['title']?>
                </div>
            <?php endforeach;?>
        </div>

</body>
</html>