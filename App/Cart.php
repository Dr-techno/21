<?php
namespace App;

class Cart
{
    protected $products = array();

    public function getProducts()
    {
        return $this->products;
    }

    public function getProduct($id)
    {
        foreach ($this->products as $product) {
            if ($product['id'] = $id) {
                return $product;
            }
        }
    }

    public function saveCart()
    {
        
        setcookie('cart', json_encode($this->products), time() + 60 * 60 * 2);
    }

    public function addProduct($cart)
    {
        array_push($this->products, $cart);
    }

    public function __construct()
    {
        $cookie = $_COOKIE['cart'];

        if (isset($cookie)) {
            $cart = json_decode($cookie, true); //преобразуем куки в массив


            if ($cart[1] == NULL) { //Если не сущетвует 2й товар добавить в продукты первый продукт

                $this->products[] = $cart;
            } else {

                foreach ($cart as $products) {//Преобразовал индексы массива в отдельные массивы и по очереди добавил в свойство
                    $this->products[] = $products;
                }
            }
        }
    }
}
